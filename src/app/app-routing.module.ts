import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { ResourcesComponent } from './resources/resources.component';
import { AboutComponent } from './about/about.component';
import { GamePlayComponent } from './game-play/game-play.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'leaderboard', component:LeaderboardComponent},
  {path: 'resources', component:ResourcesComponent},
  {path: 'about', component:AboutComponent},
  {path: 'gameplay', component:GamePlayComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent,LeaderboardComponent,ResourcesComponent,AboutComponent,GamePlayComponent,PrivacyPolicyComponent]