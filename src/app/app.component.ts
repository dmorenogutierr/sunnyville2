import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import 'firebase/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sunnyville';

  public _userList: User[] | undefined;

  constructor(private db: AngularFireDatabase) {
  // this.db.object('UsersInfo/5').set({character: 3, coins: 18, username: "sandy13",lastLogin: Date.now()});
    // this.db.object('UsersInfo/6').set({character: 5, coins: 8, username: "JazzyHelper",lastLogin:     Date.now()});
    // this.db.object('UsersInfo/7').set({character: 4, coins: 2, username: "MarkyMark", lastLogin:     Date.now()});
    // this.db.object('UsersInfo/8').set({character: 2, coins: 7, username: "DianaGoat",lastLogin:     Date.now()});
    // this.db.object('UsersInfo/9').set({character: 1, coins: 6, username: "ClimateSav0r",lastLogin:     Date.now()});
    this.getStarted();
  }

  async getStarted(){
    var users: User[] | undefined;
    await this.getUsersFromRealtimeDB().then(value => {
      users = value as User[];
    });

    this._userList = users;
    // console.log(this._userList);
  }

  getUsersFromRealtimeDB(){
    return new Promise((resolve,reject)=>{
      this.db.list('UsersInfo').valueChanges().subscribe(value =>{
        resolve(value);
      });
    });
  }

}


class User{
  Email : string | undefined;
  FirstName: string | undefined;
  LastName: string | undefined;
}

