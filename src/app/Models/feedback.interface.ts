export interface Feedback{
    $id: number;
    name: string;
    subject: string;
    email: string;
    message: string;
}