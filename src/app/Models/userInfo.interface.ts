export interface UserInfo{
    username: string;
    coins: number;
    character: number;
    lastLogin: Date;
    month: number;
    year: number;
}