import { Component, OnInit } from '@angular/core';
import { UserInfo } from '../Models/userInfo.interface';
import { UserInfoService } from '../service/user-info.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public _total_coins: number |undefined;

  constructor(public userInfoService: UserInfoService) { }

  ngOnInit(): void {
    this.userInfoService.getStarted();
    this.userInfoService.getTotalCoins().then(data => this._total_coins = data);
  }

}
