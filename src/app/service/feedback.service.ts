import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase , AngularFireList} from '@angular/fire/compat/database';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Feedback } from '../Models/feedback.interface';


@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  // feedback!: Observable<Feedback[]>;

  private feedbackCollection! : AngularFirestoreCollection<Feedback>;

  constructor(private firebase: AngularFireDatabase) { }

  public _feedbackList: Feedback[] | undefined;
  public _email: string | undefined;
  public _id: number =0;
  public _name: string| undefined;
  public _subject: string| undefined;
  public _message: string | undefined;
  public _acknowledged: number = 0;

  feedbackList?: AngularFireList <any>;

  async getStarted(){
    var feedback: Feedback[] | undefined;
    await this.getFeedback().then(value => {
      feedback = value as Feedback[];
    });

    this._feedbackList = feedback;
    // console.log(this._feedbackList);
    this._id = (this._feedbackList!.length) +1;
    console.log(this._id)
  }

    async getFeedback(){
    return new Promise((resolve,reject)=>{
      this.firebase.list('UserFeedback').valueChanges().subscribe(value =>{
        resolve(value);
      });
    });
  }

  async insertFeedback(feedback: { id: any, email: any; name: any; subject: any; message: any; acknowledged: any; }){
   this.getStarted();
    var data = {
      id: this._id!,
      email : feedback.email ,
      name: feedback.name,
      subject: feedback.subject,
      message: feedback.message,
      acknowledged: feedback.acknowledged
    }
    console.log(data);
    await this.firebase.object('UserFeedback/' + (String(data.id))).set(data);
  }
}
