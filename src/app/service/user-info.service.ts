import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase , AngularFireList} from '@angular/fire/compat/database';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserInfo } from '../Models/userInfo.interface';
import {forEach, sum} from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  
  private userinfoCollection! : AngularFirestoreCollection<UserInfo>;

  constructor(private firebase: AngularFireDatabase) { }

  public _userinfoList: UserInfo[] = [];
  public _userseasonalList: UserInfo[] = [];
  public _username: string | undefined;
  public _coins: number =0;
  public _character: number=0;
  public _total_coins: number = 0;
  public currentMonth: number = new Date().getMonth() + 1; 
  public currentYear: number = new Date().getFullYear();
  public currentPaddedMonth: string = `${this.currentMonth}-${this.currentYear}`;

  userinfoList?: AngularFireList <any>;

  async getStarted(){

    var userInfo: UserInfo[] =[];
    await this.getUserInfo().then(value => {
      userInfo = value as UserInfo[];
    });

    this._userinfoList = userInfo;
    this._userseasonalList = userInfo;
    console.log(this.currentMonth, this.currentYear, this.currentPaddedMonth);

  }

  async getUserInfo(){
  return new Promise((resolve,reject)=>{
      this.firebase.list('UsersInfo',ref => ref.orderByChild('coins')).valueChanges().subscribe(value =>{
        resolve(value);
      });
    });
  }

  async getTopTen(){
    return new Promise((resolve,reject)=>{
        this.firebase.list('UsersInfo',ref => ref.orderByChild('coins').limitToLast(10)).valueChanges().subscribe(value =>{
          resolve(value);
        });
      });
    }

    async getSeasonalTopTen(){
      return new Promise((resolve,reject)=>{
          this.firebase.list('UsersInfo',ref => ref.orderByChild('month').equalTo(this.currentPaddedMonth)).valueChanges().subscribe(value =>{
            resolve(value);
          });
        });
      }
  
  async getOrderedTopTen(){
    var userInfo: UserInfo[] =[];

    await this.getTopTen().then(value => {
      userInfo = value as UserInfo[];
    });
    
    return(userInfo.reverse());

  }


  async getTotalCoins(){
    var userInfo: UserInfo[] =[];
    var counts: number = 0;
    await this.getUserInfo().then(value => {
      userInfo = value as UserInfo[];
    });

    this._userinfoList = userInfo;
    // console.log(this._userinfoList);

    let coins_t : number[] = [];
    this._userinfoList!.forEach(value => {
      coins_t.push(value.coins);
    });
    console.log(sum(coins_t));
    counts = sum(coins_t);
    return (counts);
  }


}
