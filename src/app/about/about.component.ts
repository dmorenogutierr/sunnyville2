import { Component, OnInit } from '@angular/core';
// import { AngularFireDatabase } from '@angular/fire/compat/database';
// import 'firebase/database';
import { FeedbackService } from '../service/feedback.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback } from '../Models/feedback.interface';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  // public _feedbackList: Feedback[] | undefined;
  // public _email: string | undefined;
  // public _id: number | undefined;
  // public _FirstName: string| undefined;
  // public _LastName: string| undefined;
  // public _message: string | undefined;
  // public _acknowledged: number = 0;

  feedbackForm!: FormGroup ;
  feedback?: Feedback;
  constructor(public feedbackService: FeedbackService, private fb: FormBuilder) { 
    // this.getStarted();
    this.initForm();

  }
  submitted: boolean | undefined ;
  showSuccessMessage: boolean | undefined;
  // formControls = this.feedbackService.form.controls;
  private isEmail ='^(.+)@(.+)$';

  ngOnInit(): void {
    this.feedbackService.getStarted();
  }


  onSubmit(){
    this.submitted = true;


    if (this.feedbackForm.valid){
      console.log("saved",this.feedbackForm.value)

      if(this.feedbackForm.get('$id')?.value == null ){
        this.feedbackService.insertFeedback(this.feedbackForm.value);
        this.feedbackForm.reset();

      }

     }
  }

  isValidField(field: string): string{
    const validateField = this.feedbackForm.get(field)!;
    return (!validateField.valid && validateField.touched)
    ? 'is-invalid' : validateField?.touched ? 'is-valid' : '';

  }

  private initForm(): void{
    this.feedbackForm = this.fb.group({
      email: ['',[Validators.required, Validators.pattern(this.isEmail)]],
      name: ['',Validators.required],
      subject: ['',Validators.required],
      message: ['',[Validators.required,Validators.minLength(20)]],
      acknowledged: ['0']
    } );
  }
}


