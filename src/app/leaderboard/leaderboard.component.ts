import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import 'firebase/database';
import { FeedbackService } from '../service/feedback.service';
import { UserInfoService } from '../service/user-info.service';
import { UserInfo } from '../Models/userInfo.interface';
// import { userInfo } from 'os';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  public _userinfoList: UserInfo[] = [];
  public _userseasonalList: UserInfo[] = [];
  public _total_coins: number |undefined;
  userinfoList?: AngularFireList <any>;

  constructor(public userInfoService: UserInfoService) { }

  ngOnInit(): void {
    this.userInfoService.getStarted();
    this.userInfoService.getTotalCoins().then(data => this._total_coins = data);
    this.userInfoService.getOrderedTopTen().then(data => {
      this._userinfoList = data as UserInfo[]
      // console.log(this._userinfoList);
    });
    this.userInfoService.getSeasonalTopTen().then(data => {
      this._userseasonalList = data as UserInfo[]
      this._userseasonalList.sort((a, b) => (a.coins < b.coins) ? 1 : -1)
      if (this._userseasonalList.length > 9 ){
        this._userseasonalList.slice(0,9);
        console.log(this._userseasonalList);
      }
      console.log(this._userseasonalList)
    });
  }
}


class User{
  character : number | undefined;
  coins: number | undefined;
  username: string | undefined;
  lastLogin: Date |undefined;
  month: number |undefined;
  year: number | undefined;
}

